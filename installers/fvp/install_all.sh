#!/bin/bash

echo "Running install for FVPs"
 
for tarfile in ./*.tgz; do
    mkdir -p ./current
    tar -xvf ${tarfile} -C ./current
    pushd ./current/
    FVP_TYPE="$(awk -F_ '{print $2}' <<<"${tarfile}")" 
    FVP_ID="$(awk -F_ '{print $3}' <<<"${tarfile}")" 
    # E.g Morrello FVP filename does not include a second identifier, so we just call it a board
    if [[ "$FVP_ID" =~ [0-9]+\.[0-9]+ ]]; then
        FVP_ID="Board"
    fi
    FVP_NAME="${FVP_TYPE}_${FVP_ID}"
    bash *.sh \
        --i-agree-to-the-contained-eula \
        -d /sim/fvp/${FVP_NAME}/ \
        --no-interactive 
    popd
    rm -Rf current/*
done

