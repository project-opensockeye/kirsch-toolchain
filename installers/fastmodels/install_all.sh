#!/bin/bash
 
for tarfile in ./*.tgz; do
    mkdir -p ./current
    tar -xvf ${tarfile} -C ./current
    pushd current/$(basename ${tarfile} .tgz)
    ./setup.bin \
        --i-accept-the-license-agreement \
        --basepath /sim/fastmodels/
    popd
    rm -Rf current/*
done