# The Kirsch toolchain docker image

This repository contains a `Dockerfile` for building the Kirsch toolchain image.

A detailed description of how to use this container can be found in the [Kirsch
handbook](https://sockeye.ethz.ch/kirsch/docs/).
