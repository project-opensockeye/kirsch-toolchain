# Supported architectures are amd64 and arm64.
# Per default uses the architecture specified in "--platform OS/ARCH"
ARG arch=$TARGETARCH

# Distributability argument: Supported are free and restricted
# free: You're building a FREELY DISTRIBUTABLE version, that will NOT contain software whose distribution is limited. (E.g. WON'T contain ARM FastModels)
# restricted: You're building a RESTRICTED version that CANNOT be distributed freely. This is a SUPERSET of the free version.
ARG distributability=free

FROM ubuntu:22.04 as git-tool-base

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
  git \
  && rm -rf /var/lib/apt/lists/*


FROM git-tool-base as morello-tools-downloader

WORKDIR /tools

RUN git clone "https://git.morello-project.org/morello/fvp-firmware.git"

# Commit 36b18c8e4ea7bdcd30aed03c5d6235fbb0f8213c was confirmed to be working
RUN git clone --recurse-submodules \
  https://github.com/riscv-software-src/riscv-tests && \
  cd riscv-tests && git checkout 36b18c8e4ea7bdcd30aed03c5d6235fbb0f8213c

###########################################################################
# Clone and check out our chosen revision of the CHERI LLVM fork.
#
# We pin commit ed9d9964fb200af225739a89bfb988cbe8d8f69e of the CHERI LLVM
# fork, which has been confirmed to compile cheriette correctly
FROM git-tool-base as llvm-builder-riscv

WORKDIR /tools_temp

RUN \
  git clone --shallow-since=2023-01-01 \
  https://github.com/CTSRD-CHERI/llvm-project.git && \
  cd llvm-project && \
  git checkout df2e7bd3e17c1d6a6bc2e2f8c40488ff6c4d1db7 
  #git checkout 578ea4f7ef67d589f0ca7d10ec9e383333567421

WORKDIR /tools_temp/llvm-project

RUN apt-get update && apt-get install -y \
  clang \
  cmake \
  ninja-build \
  python3 \
  zlib1g-dev \
  axel \
  && rm -rf /var/lib/apt/lists/*

ARG llvm_install="/usr/local"
ARG target="riscv64-none-elf"
ARG llvm_target="RISCV"

RUN \
  CC=clang \
  CXX=clang++ \
  cmake -S llvm -B build -G Ninja \
  -DLLVM_TARGETS_TO_BUILD=${llvm_target} \
  -DLLVM_ENABLE_PROJECTS="clang;lld;lldb;libcxx;libcxxabi" \
  -DCMAKE_BUILD_TYPE="Release" \
  -DCMAKE_INSTALL_PREFIX=${llvm_install} \
  -DBUILD_SHARED_LIBS:BOOL=ON \
  -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=ON \
  -DLLVM_ENABLE_EXPENSIVE_CHECKS=OFF \
  -DLLVM_ENABLE_ASSERTIONS:BOOL=ON \
  -DLLVM_ENABLE_UNWIND_TABLES:BOOL=OFF \
  -DLLVM_ENABLE_EH:BOOL=ON \
  -DLLVM_ENABLE_RTTI:BOOL=ON \
  -DLLVM_ENABLE_Z3_SOLVER:BOOL=OFF

WORKDIR /tools_temp/llvm-project/build
RUN ninja
RUN ninja install

WORKDIR /tools

##############################################
FROM git-tool-base as llvm-builder-morello-base

WORKDIR /tools_temp

# Revert back to the official baremetal release.
#RUN \
#  git clone --shallow-since=2024-01-01 \
#  https://git.morello-project.org/morello/llvm-project.git && \
#  cd llvm-project && \
#  git branch morello/release-1.8

WORKDIR /tools_temp/llvm-project

RUN apt-get update && apt-get install -y \
  clang \
  cmake \
  ninja-build \
  python3 \
  python3.10-distutils \
  zlib1g-dev \
  axel \
  && rm -rf /var/lib/apt/lists/*

# Revert back to the official baremetal release.
#ARG llvm_install="/tools/llvm-morello"
#ARG target="aarch64-none-elf"
#ARG llvm_target="AArch64"
#
## We compile LLVM for Morello.
## This has the advantage that ARM users can now compile for Morello.
#RUN \
#  CC=clang \
#  CXX=clang++ \
#  cmake -S llvm -B build -G Ninja \
#  -DLLVM_TARGETS_TO_BUILD=${llvm_target} \
#  -DLLVM_ENABLE_PROJECTS="clang;lld;lldb;libc;libcxx;libcxxabi;compiler-rt" \
#  -DCMAKE_BUILD_TYPE="Release" \
#  -DCMAKE_INSTALL_PREFIX=${llvm_install} \
#  -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=ON \
#  -DBUILD_SHARED_LIBS:BOOL=ON \
#  -DLLVM_ENABLE_EXPENSIVE_CHECKS=OFF \
#  -DLLVM_ENABLE_ASSERTIONS:BOOL=ON \
#  -DLLVM_ENABLE_EH:BOOL=ON \
#  -DLLVM_ENABLE_UNWIND_TABLES:BOOL=OFF \
#  -DLLVM_ENABLE_RTTI:BOOL=ON \
#  -DLLVM_ENABLE_Z3_SOLVER:BOOL=OFF
#
#WORKDIR /tools_temp/llvm-project/build
#RUN ninja
#RUN ninja install

# The following is a skeleton implementation to compile compiler-rt
# IF we ever need it custom-build, build on this:
# https://www.llvm.org/docs/HowToCrossCompileBuiltinsOnArm.html
#
#ARG march="morello"
#ARG mabi="purecap"
#ARG build_c_flags="--target=${target} -march=${march} -mabi=${mabi}"
#
#WORKDIR /tools_temp/llvm-project
#RUN rm -Rf build/
#RUN cmake -S compiler-rt -B build -G Ninja \
#  -DCMAKE_AR=${llvm_install}/bin/llvm-ar \
#  -DCMAKE_ASM_COMPILER_TARGET=${target} \
#  -DCMAKE_ASM_FLAGS="${build_c_flags}" \
#  -DCMAKE_C_COMPILER=${llvm_install}/bin/clang \
#  -DCMAKE_C_COMPILER_TARGET=${target} \
#  -DCMAKE_C_FLAGS="${build_c_flags}" \
#  -DCMAKE_EXE_LINKER_FLAGS="-fuse-ld=lld" \
#  -DCMAKE_NM=${llvm_install}/bin/llvm-nm \
#  -DCMAKE_RANLIB=${llvm_install}/bin/llvm-ranlib \
#  -DCOMPILER_RT_OS_DIR="baremetal" \
#  -DCOMPILER_RT_BAREMETAL_BUILD=ON \
#  -DCOMPILER_RT_DEFAULT_TARGET_ONLY=ON \
#  -DCOMPILER_RT_INCLUDE_TESTS=OFF \
#  -DCOMPILER_RT_BUILD_BUILTINS=ON \
#  -DCOMPILER_RT_BUILD_LIBFUZZER=OFF \
#  -DCOMPILER_RT_BUILD_MEMPROF=OFF \
#  -DCOMPILER_RT_BUILD_PROFILE=OFF \
#  -DCOMPILER_RT_BUILD_SANITIZERS=OFF \
#  -DCOMPILER_RT_BUILD_XRAY=OFF \
#  -DCOMPILER_RT_INCLUDE_TESTS=OFF \
#  -DLLVM_CONFIG_PATH=${llvm_install}/bin/llvm-config

WORKDIR /tools

FROM llvm-builder-morello-base AS llvm-builder-morello-amd64

WORKDIR /installers
RUN axel ${axelargs} -o llvm-morello.tgz "https://git.morello-project.org/morello/llvm-project-releases/-/archive/morello/baremetal-release-1.8/llvm-project-releases-morello-baremetal-release-1.8.tar.gz"

WORKDIR /tools/llvm-morello-distribution
RUN tar -xv --strip-components=1 --directory=./ --file=/installers/llvm-morello.tgz

# Download for arm64
FROM llvm-builder-morello-base AS llvm-builder-morello-arm64
# No baremetal toolchain exists for arm64 
WORKDIR /installers

FROM llvm-builder-morello-${arch} AS llvm-builder-morello


###########################################################################
FROM git.morello-project.org:5050/morello/morello-firmware-docker/morello-firmware-docker:latest AS firmware-builder-morello 
# https://git.morello-project.org/morello/docs/-/blob/morello/mainline/firmware/user-guide.rst

WORKDIR /tools/morello-firmware-fvp

RUN repo init \
  -u https://git.morello-project.org/morello/manifest.git \
  --depth=1 \
  -b morello/integration-1.6 \
  -g bsp

RUN repo sync

# -p <fvp/soc>
# -f <none/busybox>
RUN bash ./build-scripts/build-all.sh \
  -p fvp \
  -f none \
  build

WORKDIR /tools/morello-firmware-soc

RUN repo init \
  -u https://git.morello-project.org/morello/manifest.git \
  --depth=1 \
  -b morello/integration-1.6 \
  -g bsp

RUN repo sync

# -p <fvp/soc>
# -f <none/busybox>
RUN bash ./build-scripts/build-all.sh \
  -p soc \
  -f none \
  build

###########################################################################
FROM git-tool-base AS simulator-builder

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install --no-install-recommends -y \
  build-essential \
  ca-certificates \
  device-tree-compiler \
  libgmp-dev \
  pkg-config \
  opam \
  z3 \
  zlib1g-dev \
  && rm -rf /var/lib/apt/lists/*

RUN opam init --bare --disable-sandboxing
RUN opam switch create 4.14.1
RUN opam pin --yes add sail 0.17.1

WORKDIR /sim

RUN git clone \
  --recursive --depth=1 \
  https://github.com/CTSRD-CHERI/sail-cheri-riscv && \
  cd sail-cheri-riscv && \
  git checkout c93d5eff4132fd52ddad32020cedae93573bfa29

WORKDIR /sim/sail-cheri-riscv

RUN make csim
RUN eval $(opam env) && make osim

###########################################################################
# We can only automatically download publicly available FVPs.
# For Fast Models and FVPs with limited distribution, please download them
# manually and place them in the ./installers/ directory.
# Please see the README.md or Handbook for details
FROM ubuntu:22.04 AS axel-downloader-base

RUN if [[ -z "$arch" ]]; then echo "Argument arch not provided! Options: arm64 | amd64" ; else echo "arch: ${arch}" ; fi
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install --no-install-recommends -y \
  build-essential \
  ca-certificates \
  pkg-config \
  lsb \
  libsm6 \
  libxcursor1 \
  libxft2 \
  libxrandr2 \
  libxt6 \ 
  libxinerama1 \
  axel \
  && rm -rf /var/lib/apt/lists/*

# Force creation of directories
WORKDIR /sim/fastmodels
WORKDIR /sim/fvp

ADD installers /installers

ARG axelargs="-n 6 --percentage"

# Building for x86_64
FROM axel-downloader-base AS fastmodels-builder-amd64-free

WORKDIR /installers/fvp
# Download the standard set of FVPs
RUN axel ${axelargs} "https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/OSS/FVP/Corstone-315/FVP_Corstone_SSE-315_11.24_22_Linux64.tgz?rev=40cc82353ada4ab0865f9be8ed087e17&hash=AF0AE5C0CBDC1AA19B015BB8B81305A8E4A91CFA"
RUN axel ${axelargs} "https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/OSS/FVP/Corstone-310/FVP_Corstone_SSE-310_11.24_13_Linux64.tgz?rev=c370b571bdff42d3a0152471eca3d798&hash=1E388EE3B6E8F675D02D2832DBE61946DEC0386A"
RUN axel ${axelargs} "https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/OSS/FVP/Corstone-1000/FVP_Corstone_1000_11.23_25_Linux64.tgz?rev=e6ac7624247540029ca530646ca16e25&hash=A00FBF86D9B2A9DF4631D20DB8692241F83C0553"
RUN axel ${axelargs} "https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/OSS/FVP/Corstone-500/FVP_Corstone_500_11.22_35_Linux64.tgz?rev=c6228d46c6a24a459657666716ceaf25&hash=1B237862BE513B9FCE5A2D10FF96A27912331B7C"
RUN axel ${axelargs} "https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/OSS/FVP/Morello%20Platform/FVP_Morello_0.11_34.tgz?rev=5f34837ae6c14ede8493dfc24c9af397&hash=862883120C5638E0B3C5ACA6FDDC5558021E1886"

# Building for aarch64
FROM axel-downloader-base AS fastmodels-builder-arm64-free

WORKDIR /installers/fvp
# Download the standard set of FVPs
RUN axel ${axelargs} "https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/OSS/FVP/Corstone-315/FVP_Corstone_SSE-315_11.24_22_Linux64_armv8l.tgz?rev=2573a984107d4187aaea974fd8bc943e&hash=AD8129C2D0E0506FC58B55E9561D956EFD023F70"
RUN axel ${axelargs} "https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/OSS/FVP/Corstone-310/FVP_Corstone_SSE-310_11.24_13_Linux64_armv8l.tgz?rev=62f95ef8d82b4be4882906d911ccacbe&hash=2E9E6F2038BE85E6CEEBAC68F4EEF5EA44B08370"
RUN axel ${axelargs} "https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/OSS/FVP/Corstone-1000/FVP_Corstone_1000_11.23_25_Linux64_armv8l.tgz?rev=b43b99d18f464067a4bc3138ae02ee42&hash=DBB8999EB28865D2234D9F75103E2EE1D8DEF116"
RUN axel ${axelargs} "https://developer.arm.com/-/media/Arm%20Developer%20Community/Downloads/OSS/FVP/Corstone-500/FVP_Corstone_500_11.22_35_Linux64_armv8l.tgz?rev=93cdb5d1a5094219b714682bc742f4ce&hash=2BCF5694DC3931B709FC12DA718F2403D4DB6077"

# Finishing in common
FROM fastmodels-builder-${arch}-free AS fastmodels-builder-free

WORKDIR /installers/fvp
RUN bash -x install_all.sh 

# Finishing in common
FROM fastmodels-builder-free AS fastmodels-builder-restricted

ADD installers/fastmodels/FastModels_11-25-015_Linux64.tgz /installer/fastmodels
WORKDIR /installer/fastmodels/FastModels_11-25-015_Linux64

RUN bash setup.sh \
  --i-accept-the-license-agreement \
  --basepath /sim/fastmodels/

ADD installers/fastmodels/FastModels_ThirdParty_IP_11-25_b15_Linux64.tgz /installers/fastmodels_third_party
WORKDIR /installers/fastmodels_third_party/FastModels_ThirdParty_IP_11-25_Linux64

RUN ./setup.bin \
  --i-accept-the-license-agreement \
  --basepath /sim/fastmodels/

FROM fastmodels-builder-${distributability} AS fastmodels-builder


# ============================= arm-gnu-toolchain ==============================

# https://developer.arm.com/downloads/-/arm-gnu-toolchain-downloads

FROM axel-downloader-base as arm-gnu-toolchain-downloader-amd64
WORKDIR /installers
RUN axel ${axelargs} "https://developer.arm.com/-/media/Files/downloads/gnu/11.3.rel1/binrel/arm-gnu-toolchain-11.3.rel1-x86_64-aarch64-none-elf.tar.xz?rev=73ff9780c12348b1b6772a1f54ab4bb3&hash=B80470312E67CF1F157D0A883FDEB279"
WORKDIR /tools/arm-gnu-toolchain
RUN tar -xf /installers/arm-gnu-toolchain-11.3.rel1-x86_64-aarch64-none-elf.tar.xz --strip-components=1

FROM axel-downloader-base as arm-gnu-toolchain-downloader-arm64
WORKDIR /installers
RUN axel ${axelargs} "https://developer.arm.com/-/media/Files/downloads/gnu/11.3.rel1/binrel/arm-gnu-toolchain-11.3.rel1-aarch64-aarch64-none-elf.tar.xz?rev=d5fde996ea81478e96de9713fcf44b1c&hash=367E45E55900FF69ABBF990D538F56E8"
WORKDIR /tools/arm-gnu-toolchain
RUN tar -xf /installers/arm-gnu-toolchain-11.3.rel1-x86_64-aarch64-none-elf.tar.xz --strip-components=1

FROM arm-gnu-toolchain-downloader-${arch} AS arm-gnu-toolchain-downloader

FROM python:3.11-alpine AS handbook-builder

RUN apk add git

RUN git clone --depth=1 https://gitlab.inf.ethz.ch/project-opensockeye/kirsch-handbook /handbook

FROM rust:1.81-bullseye AS sockeye3-builder

RUN apt-get update && apt-get install -y --no-install-recommends \
  cmake \
  clang \
  g++ \
  git \
  && rm -rf /var/lib/apt/lists/*

RUN git clone --depth=1 https://gitlab.inf.ethz.ch/project-opensockeye/sockeye-specifications /specs

RUN git clone --depth=1 https://gitlab.inf.ethz.ch/project-opensockeye/sockeye3 /sockeye3
WORKDIR /sockeye3
RUN cargo build

FROM rust:1.81-bullseye AS mackerel2-builder

RUN apt-get update && apt-get install -y --no-install-recommends \
  cmake \
  clang \
  g++ \
  git \
  && rm -rf /var/lib/apt/lists/*

RUN git clone --depth=1 https://gitlab.inf.ethz.ch/project-opensockeye/mackerel2 /mackerel2
WORKDIR /mackerel2
RUN cargo build --release

FROM git-tool-base AS riscv-qemu-builder

WORKDIR /riscv-qemu

RUN git clone --shallow-since=2023-01-01 \
  https://github.com/CTSRD-CHERI/qemu.git

WORKDIR /riscv-qemu/qemu/build/

RUN apt-get update && apt-get install -y --no-install-recommends \
  make \ 
  python3-minimal \
  ninja-build \
  libtool pkg-config autotools-dev automake autoconf libglib2.0-dev libpixman-1-dev \
  bison groff-base libarchive-dev flex \
  libncurses-dev \
  && rm -rf /var/lib/apt/lists/*

# This needs to be a separate apt-get command, as otherwise
# `cc` is not set.
RUN apt-get update && apt-get install -y --no-install-recommends \
  clang-12 \
  lld-12 \
# Explicit additional packages:
  libclang-common-12-dev libclang-cpp12 libclang1-12 \
  libllvm12 llvm-12 llvm-12-dev \
  llvm-12-linker-tools llvm-12-runtime llvm-12-tools \
  && rm -rf /var/lib/apt/lists/*

RUN ../configure \
  --disable-sdl \
  --disable-gtk \
  --disable-opengl \
  --disable-virglrenderer \
  --disable-stack-protector \
# Capstone disassembler not support CHERI
  --disable-capstone \
# Linux/BSD user is not supported for CHERI (yet)
  --disable-bsd-user \
  --disable-linux-user \
# Optimize as much as possible
  --disable-pie \
  --disable-strip \ 
# Target linux
  --disable-linux-aio \
  --disable-kvm \
# Extend
  --target-list=riscv64cheri-softmmu \
  --disable-xen \
  --disable-docs \
  --disable-rdma \
  --disable-werror

RUN make

# This is a staged version of the final dev container.
# We squash it down to a single stage below.
FROM ubuntu:22.04 as final

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install --no-install-recommends -y \
  build-essential \
  ca-certificates \
  device-tree-compiler \
  file \
  git \
  libgmp10 \
  make \
  ninja-build \
  python3 \
  python3-venv \
  python3-pip \
  zlib1g \
  libtinfo5 \
  libdbus-1-dev \ 
  libsystemd-dev \
  tigervnc-standalone-server \
  tigervnc-xorg-extension \
  tigervnc-viewer \
  tigervnc-tools \
  xterm \
  openbox \
  obconf \
  openbox-menu \
  telnet \
  texlive-latex-recommended \
  tex-gyre \
  fonts-lato \
  texlive-latex-extra \
  latexmk \
  less \
  fish \ 
  ripgrep \
  musl-tools \
  musl-dev \
  cmake \
  libxml2-utils \
  xxd \
  && rm -rf /var/lib/apt/lists/*

# We need --install-recommends here because texlive-fonts-extra needs to install all the other
# font packages too...
RUN apt-get update && apt-get install --install-recommends -y \
  texlive-fonts-extra \
  && rm -rf /var/lib/apt/lists/*

COPY --from=llvm-builder-riscv /usr/local/ /usr/local/
COPY --from=llvm-builder-riscv /tools/ /tools/
COPY --from=llvm-builder-morello /tools/ /tools/
COPY --from=firmware-builder-morello /tools/ /tools/
COPY --from=simulator-builder /sim /sim
COPY --from=fastmodels-builder /sim /sim
COPY --from=simulator-builder /sim/sail-cheri-riscv/c_emulator/cheri_riscv_sim_RV64 /sim/cheri_riscv_sim_RV64
COPY --from=simulator-builder /sim/sail-cheri-riscv/ocaml_emulator/cheri_riscv_ocaml_sim_RV64 /sim/cheri_riscv_ocaml_sim_RV64
COPY --from=sockeye3-builder /specs /specs
COPY --from=sockeye3-builder /sockeye3/target/debug/sockeye3 /tools/sockeye3
COPY --from=sockeye3-builder /sockeye3/contrib/generate-pagetables.py /tools/sockeye3-generate-pagetables.py
COPY --from=mackerel2-builder /mackerel2/target/release/mackerel2 /tools/mackerel2
COPY --from=fastmodels-builder /sim/fastmodels /sim/fastmodels
COPY --from=fastmodels-builder /sim/fvp /sim/fvp
COPY --from=morello-tools-downloader /tools/ /tools/
COPY --from=arm-gnu-toolchain-downloader /tools/ /tools/
COPY --from=riscv-qemu-builder /riscv-qemu/qemu/build/qemu-system-riscv64cheri /tools/qemu-system-riscv64cheri
COPY --from=handbook-builder /handbook /handbook

RUN python3 -m pip install -r /handbook/requirements.txt

###########################################################################
# This is the actual dev container that runs.
FROM scratch

# SQUASH IT DOWN
LABEL org.opencontainers.image.authors="NetOS Group, ETH Zurich"

LABEL version="0.5"
LABEL description="Kirsch Toolchain for ${arch} with distributability ${distributability}"

COPY --from=final / /

WORKDIR /src
